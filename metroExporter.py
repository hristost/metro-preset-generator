from music21 import *
from fractions import Fraction
from collections import namedtuple

# Note lengths: music21-->constant names conversion
# TODO: Move to separate file

metroPreset = namedtuple("metroPreset", "pitch harmony duration")


durationNames =     {"ktWhole":4,   "ktHalf":2,         "ktQuarter":1,
                    "kt8th":0.5,    "kt16th":0.25,      "kt32nd":0.125,
                    "ktWholeTrip":  Fraction(4, 3),
                    "ktHalfTrip":   Fraction(2, 3),
                    "ktQuarterTrip":Fraction(1, 3),
                    "kt8thTrip":    Fraction(1, 6),
                    "kt16thTrip":   Fraction(1, 12),
                    "ktDotHalf":    3,
                    "ktDot4th":     1.5,
                    "ktDot8th":     0.75,
                    "ktDot16th":    0.375,
                    "ktDot32nd":    0.1865}

def metroNoteDurationName(n):
    '''The duration of the note `n` expressed with a constant name'''
    return metroDurationName(n.duration)

def metroDurationLengthName(n):
    '''The duration `n` expressed with a constant name'''
    eps = 0.0001
    for name in durationNames:
        l = durationNames[name]
        if n>l-eps and n<l+eps:
            return name
    print "Not found: " + str(n)
    return "kt8th"
    #TODO: Impl. tie()
    raise Exception("Note length not present in dictorary")
#TODO: Refactor
def metroDurationName(n):
    '''The duration `n` expressed with a constant name'''
    eps = 0.0001
    for name in durationNames:
        l = durationNames[name]
        if n.quarterLength>l-eps and n.quarterLength<l+eps:
            return name
    print "Not found: " + str(n.quarterLength)
    return "kt8th"
    #TODO: Impl. tie()
    raise Exception("Note length not present in dictorary")


class metroPreset:
    def __init__(self, stream=None):
        if stream != None:

            # We need to know the key in order to get pitches relative to it
            key = analysis.discrete.analyzeStream(stream, 'Krumhansl')
            keyRoot = key.tonic
            # Total duration of te riff
            d = 0
            # Note representations: semitones relative to the key / rests + rel.
            # harmonies
            sCodes = []
            sHarmonyCodes = []
            sVelocities = []
            # Note lengths as constant names
            dCodes = []
            dLengths = [] # for debugging


           # For each note in the pattern, append the arrays for the preset
            for n in stream:
                if n.isRest:
                    r = note.Rest(n)
                    r.duration = n.duration
                    sCodes.append("kRest")
                    sHarmonyCodes.append(0)
                    sVelocities.append(0)
                elif type(n) is note.Note:
                    sCodes.append(n.midi-keyRoot.midi)
                    sHarmonyCodes.append(0)
                    sVelocities.append(n.volume.velocity)
                elif type(n) is chord.Chord:
                    topNote = note.Note(n.pitches[-1])
                    harmonyNote = note.Note(n.pitches[0])
                    sCodes.append(topNote.midi-keyRoot.midi)
                    sHarmonyCodes.append(harmonyNote.midi-topNote.midi)
                    sVelocities.append(n.volume.velocity)

                dCodes.append(metroNoteDurationName(n))
                dLengths.append(n.duration.quarterLength)
                d += n.duration.quarterLength
                # TODO: sum durations & find bar duration
            self.pitches = sCodes
            self.harmonies = sHarmonyCodes
            self.durations = dCodes
            self.velocities = sVelocities
            self.flags = ["PLFragment"]
    @classmethod
    def fromString(self, str):
        preset = metroPreset()
        arrays = str.split("=")

        (pitchStr, harmonyStr, durationStr, offsetStr, velocityStr, bendStr, headBendStr, tailBendStr, flagStr) = arrays
        preset.pitches = pitchStr.split(":")
        preset.durations = durationStr.split(":")
        preset.harmonies = harmonyStr.split(":")
        preset.velocities = velocityStr.split(":")
        preset.flags = flagStr.split(":")
        preset.headBends = headBendStr
        preset.tailBends = tailBendStr
        preset.bends = bendStr
        preset.measureOffset = offsetStr

        return preset

    def toString(self):
        string = ""
        string+=':'.join([str(x) for x in self.pitches])
        string+='='
        string+=':'.join([str(x) for x in self.harmonies])
        string+='='
        string+=':'.join([str(x) for x in self.durations])
        string+='='
        string+=self.measureOffset
        string+='='
        string+=':'.join([str(x) for x in self.velocities])
        string+='='
        string+=self.bends
        string+='='
        string+=self.headBends
        string+='='
        string+=self.tailBends
        string+='='
        string+=':'.join([str(x) for x in self.flags])
        return string


def streamFromMetroPreset(preset):
    s = stream.Stream()
    for i in range(0, len(preset.pitches)):
        (pitch, harmony, duration, velocity) = (preset.pitches[i], preset.harmonies[i], preset.durations[i], preset.velocities[i])
        durationQuarterLength = durationNames[duration]

        if pitch == "kRest":
            n = note.Rest()
            n.quarterLength = durationQuarterLength
            s.append(n)
        else:
            n = note.Note(60+int(pitch))
            n.quarterLength = durationQuarterLength
            if not harmony == 0:
                hn = note.Note(60+pitch+harmony)
                n = chord.Chord([n, hn])
            s.append(n)
    return s

