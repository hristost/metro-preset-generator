import sys
import os
import copy
from music21 import *
from fractions import Fraction
from collections import namedtuple

import metroExporter

def canPrint():
    sys.stdout = sys.__stdout__
def cantPrint():
    sys.stdout = open(os.devnull, "w")

# Path of the MIDI file we extract riffs from
filePath = "./b00153.mid" #b00153.mid.2, b00251.2
# Voice which we scan for riffs
leadVoice = 2

riffMinimumNotes = 3
riffMaximumNotes = 32
# TODO: Use duration sum based on measures

# MIDI representations of the notes
midiNotes = []
# music21 representations of the notes
notes = []

scoreEntry = namedtuple("scoreEntry", "index positionInMeasure measure music21 midi duration")
# Get the file
patterns = []

def processFile():
    global filePath, leadVoice, riffMaximumNotes, riffMinimumNotes, midiNotes, notes, scoreEntry, patterns
    s = converter.parse(filePath)
    # Get the voice we're scanning for riffs
    lead = s.parts[leadVoice]
    print "File opened"
    key = analysis.discrete.analyzeStream(s, 'Krumhansl')
    keyRoot = key.tonic

    positionInMeasure = 0
    measureLength = 4
    measureNo = 0
    # Iterate through the melody to get the notes
    for i in range(0, len(lead.elements)):
        currentElement = lead.elements[i]# Don't know if a `for in` loop can
                                         # change the order; this is safe
        # Keep track of where we are in a measure
        if type(currentElement) is meter.TimeSignature:
            measureLength = currentElement.numerator*4/currentElement.denominator
            positionInMeasure = 0
            measureNo += 1

        # Using note.offset can be faster, but risky when changing signatures
        print positionInMeasure
        positionInMeasure += currentElement.duration.quarterLength
        while positionInMeasure>measureLength:
            positionInMeasure-=measureLength
            measureNo += 1
        # Skip unrelevant objects like text, etc.
        if not (type(currentElement) is note.Note or type(currentElement) is note.Rest or type(currentElement) is chord.Chord):
            continue


        # If we have a chord, use the highest note and assume everything
        # else is bass. TODO: Include chords in presets
        if type(currentElement) is chord.Chord:
            topNote = note.Note(currentElement.pitches[-1])
            notes.append(scoreEntry(i, positionInMeasure, measureNo, currentElement, topNote.midi, currentElement.duration))
        elif currentElement.isRest:
            if len(notes)==0:
                continue
            # Copy the previous note and flag it as a rest, so we have  a
            # homogeneous array
            oldDuration = currentElement.duration
            #print oldDuration
            n = copy.copy(notes[-1].music21)
            if type(n) is chord.Chord:
                n = note.Note(n.pitches[-1])
            n.duration = oldDuration
            #print n.duration
            n.isRest = True


            notes.append(scoreEntry(i, positionInMeasure, measureNo, n, n.midi, n.duration))
        elif type(currentElement) is note.Note:
            notes.append(scoreEntry(i, positionInMeasure, measureNo, currentElement, currentElement.midi, currentElement.duration))



    # Get the derivative of the note sequence, using their MIDI codes
    # The numbers now actually represent the difference between pairs of
    # consecutive pitches in semitones
    derivativeEntry = namedtuple("derivativeEntry", "i d")
    derivative = [derivativeEntry(i, notes[i].midi - notes[i+1].midi) for i in range(0, len(notes)-1)]

    # Find patterns (slow but simple: offset the array and overlay with the
    # original to find repetitions)

    print "Start searching for patterns"
    #print derivative

    def streamFromScoreEnties(se):
        s = stream.Stream()
        for e in se:
            s.append(e.music21)
        return s

    def streamsAreAlike(s1, s2):
        matches = [s1[i].midi-s1[i+1].midi==s2[i].midi-s2[i+1].midi for i in range(0, min(len(s1), len(s2))-1)]
        matchCount = sum(1 for i in matches if i)
        return matchCount>0.8*len(matches)
    # Search for fragments
    start = 0
    startInMeasure = 0
    while notes[start].music21.isRest:
        start += 1

    for end in range(start, len(notes)):
        fragmentStream = streamFromScoreEnties(notes[start:end])
        try:
            key = analysis.discrete.analyzeStream(fragmentStream, 'Krumhansl')
        except:
            continue
        normalizedDurations = [i.duration.quarterLength/fragmentStream.duration.quarterLength for i in fragmentStream.elements]

        #print notes[end]
        endNote = notes[end].music21
        pEnd = 0

        if endNote.isRest and endNote.duration.quarterLength>1.0 and fragmentStream.duration>2 and end-start<riffMaximumNotes:
            lastNote = note.Note(notes[end-1].midi)
            if lastNote.name==key.pitches[0].name or lastNote.name==key.pitches[2].name or endNote.name==key.pitches[4].name:
                existing = False
                for p in patterns:
                    if streamsAreAlike(p['patternNotes'], notes[start:end]):
                        p['count']+=1
                        existing = True

                pattern = {"patternNotes":notes[start:end],
                            "count":            1,
                            "positionInMeasure":metroExporter.metroDurationLengthName(notes[start].positionInMeasure)}
                if not existing:
                    patterns.append(pattern)
            start = end
            while notes[start].music21.isRest:
                start += 1

