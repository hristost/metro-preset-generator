import sys

from riffDetector import *
filePath = sys.argv[1]
leadVoice = int(sys.argv[2])
processFile();

from Tkinter import *
from PIL import ImageTk, Image
import subprocess
from music21 import *

master = Tk()
currentPreset = None

Grid.columnconfigure(master, 1, weight=1)
Grid.columnconfigure(master, 2, weight=1)
Grid.columnconfigure(master, 3, weight=1)
Grid.columnconfigure(master, 4, weight=1)

patternNo = 0
patternStream = None
patternDatabase = patterns

imageView = None
img = None

binPresets = []
binDurations = []
binHarmonies = []

def accept():
    global binHarmonies
    global binDurations
    global binPresets
    global patternNo
    global bendEntry, tailBendEntry, headBendEntry, scaleEntry
    currentPreset.bends = hex(int(bendEntry.get().replace(" ", ""), 2))
    currentPreset.headBends = hex(int(headBendEntry.get().replace(" ", ""), 2))
    currentPreset.tailBends = hex(int(tailBendEntry.get().replace(" ", ""), 2))
    currentPreset.scale = scaleEntry.get()
    print currentPreset.bends
    patternNo += 1
    global currentPreset
    for opt in optionNames:
        if optionVars[opt]==True:
            currentPreset.flags.append(opt)
    with open("stage1.txt", "a") as f:
        f.write("\n"+currentPreset.toString())
    if patternNo>=len(patterns):
            sys.exit(0)
    openPattern(patternNo)
def play():
    global patternStream
    sp = midi.realtime.StreamPlayer(patternStream)
    sp.play()
    print "pf"
def reject():

    global binDurations
    global binPresets
    global patternNo
    patternNo += 1
    openPattern(patternNo)
def updateNotes():
    global imageView
    global master
    global img
    path=patternStream.write('lily.png') # TODO: Open in background TODO: Impossible
    print path
    #subprocess.call('rm n.png')
    subprocess.call('cp '+path+' ./n.png', shell=True)
    subprocess.call('sips -s format gif n.png --out ./', shell=True)


    if not imageView == None:
        pass
        #imageView.grid_remove()
    img = PhotoImage(file="n.gif")
    imageView = Label(master, image = img)
    print "imgview created"
    imageView.grid(row=0, column=0, columnspan = 5)

def updatePreset():
    global patternStream, currentPreset
    pitchString = pitchEntry.get()
    pitches = pitchString.split(", ")
    for pitch in pitches:
        if not pitch == "kRest":
            pitch = int(pitch)
    currentPreset.pitches = pitches
    patternStream = metroExporter.streamFromMetroPreset(currentPreset)
    updateNotes()

entryFields = []
entryNames = ["Pitches", "Harmonies", "Durations", "Velocities", "Bends", "HeadBends", "TailBends", "Scale"]
row = 1

for name in entryNames:
    entryFields.append(Entry(master))
    entryFields[-1].grid(row=row, column=1, sticky=E+W, columnspan=4)
    Label(master, text=name).grid(row=row, sticky=E, column = 0)
    row += 1

pitchEntry      = entryFields[0]
harmonyEntry    = entryFields[1]
durationEntry   = entryFields[2]
velocityEntry   = entryFields[3]
bendEntry       = entryFields[4]
headBendEntry   = entryFields[5]
tailBendEntry   = entryFields[6]
scaleEntry      = entryFields[7]
entryFields[7].insert(0, '0')

optionNames = ["PLPdontRepeat", "PLFragment", "PLMustAlign", "PLAlreadyUsesScalePos", "PLExpandPat", "PLBendsAreGradual", "PLTurnaround"]
optionVars = {}

for name in optionNames:
    optionVars[name] = IntVar()
    c = Checkbutton(master, text=name, variable=optionVars[name])
    c.grid(row=row, column=0, columnspan = 4, sticky=W+N+S)
    row+=1

acceptButton = Button(master, text="Accept", command=accept)
acceptButton.grid(row=row, column=4, sticky=W+E+N+S)

rejectButton = Button(master, text="Reject", command=reject)
rejectButton.grid(row=row, column=3, sticky=W+E+N+S)

playButton = Button(master, text="Play", command=play)
playButton.grid(row=row, column=0, sticky=W+N+S)
updateButton = Button(master, text="Update", command=updatePreset)
updateButton.grid(row=row, column=1, sticky=W+N+S)

def streamFromPattern(p):
    s = stream.Stream()

    for n in p['patternNotes']:
        if n.music21.isRest:
            r = note.Rest()
            r.duration = n.music21.duration
            s.append(r)
        else:
            s.append(n.music21)
    return s

img = None
def openPattern(no):
    global master
    global entryFields
    global imageView, img
    global patterns
    global patternStream, currentPreset
    pattern = patterns[no]
    # Stream used for showing the riff to the user
    s = streamFromPattern(pattern)
    currentPreset = metroExporter.metroPreset(s)
    currentPreset.measureOffset = pattern["positionInMeasure"]
    s = metroExporter.streamFromMetroPreset(currentPreset)
    patternStream = s
    offBeat = not(pattern["patternNotes"][0].positionInMeasure==0)
    for f in entryFields: f.delete(0, END)

    entryFields[0].insert(0, ', '.join([str(x) for x in currentPreset.pitches]))
    entryFields[1].insert(0, ', '.join([str(x) for x in currentPreset.harmonies]))
    entryFields[2].insert(0, ', '.join([str(x) for x in currentPreset.durations]))
    entryFields[3].insert(0, ', '.join([str(x) for x in currentPreset.velocities]))
    entryFields[4].insert(0, '0')
    entryFields[5].insert(0, '0')
    entryFields[6].insert(0, '0')

    # Check whether the riff has an acceptable length and show it
    if 5>=1.5: #TODO: Get bar length
        # Show notes for the riff
        updateNotes()


openPattern(0)
master.mainloop()


