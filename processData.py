from music21 import *

samplingBlockSize = 96      #1/96th second

def getStreamBackground(stream):
    dataLength = stream.duration.quarterLength*(samplingBlockSize/4)
    data = [[0]*dataLength]*7
    for n in stream.flat:
        if note.isRest:
            continue

        start = n.offset.quarterLength*(samplingBlockSize/4)
        duration = n.duration.quarterLength*(samplingBlockSize/4)
        end = start + duration
        data[0][start:end] = [1] * duration


