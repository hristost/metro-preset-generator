from metroExporter import *

binDurations = []
binHarmonies = []
binVelocities = []
binPresets = []

with open("stage1.txt", "r") as f:
    for row in f.readlines():
        if len(row)<3: continue
        p = metroPreset.fromString(row)

        durationIndex = len(binDurations)
        harmonyIndex = len(binHarmonies)

        binDurations.append("static const Word dpp"+str(durationIndex)+"[] = {"+str(len(p.durations))+", "+p.measureOffset+", "+", ".join(p.durations)+"};")
        binHarmonies.append("{"+', '.join([str(x) for x in p.harmonies])+"}, ")
        twelveBitScale = "0"
        bendBitMap = p.bends
        headBendBitMap = p.headBends
        tailBendBitMap = p.tailBends
        flags = p.flags
        if len(flags) == 0:
            flags = ["PLFragment"]
        flags = " | ".join(flags)
        preset = str(len(p.pitches))+", "+twelveBitScale+", "+bendBitMap+", "+headBendBitMap+", "+tailBendBitMap+", "+flags+", "
        preset+=str(harmonyIndex)+", "+str(durationIndex)+", {"+', '.join([str(x) for x in p.pitches])+"}, "
        binPresets.append(preset)

print "static const harmonypresetTable PLHarmonyTable[] = {"
for h in binHarmonies:
    print h
print "}"

print "static const plpreset PLPresets[] = {"
for p in binPresets:
    print p
print "}"

for d in binDurations:
    print d
print "static const dpp dp3s[] = {" + ", ".join(["(dpp)dpp"+str(x) for x in range(0, len(binDurations))])+"};"
